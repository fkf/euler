﻿using System;
using System.IO;
using System.Linq;
using System.Numerics;

namespace euler
{
    /// <summary>
    /// TODO: Document Task22
    /// Author: us-fifi
    /// Date: 30.12.2014 09:27:45
    /// </summary>
    public class Task22 : ITask
    {
        public void Calculate()
        {
            var txt = File.ReadAllText(@"..\..\resources\p022_names.txt");
            txt=txt.Replace("\"", "");
            var lines = txt.Split(',');
            Array.Sort(lines, StringComparer.InvariantCulture);
            BigInteger sum = 0;
            for (int i = 0; i < lines.Length; i++)
            {
                sum += lines[i].ToCharArray().Sum(x => (int) x - 64)*(i + 1);
            }
            Console.WriteLine("Found {0}", sum);
        }
    }
}