﻿using System;

namespace euler
{
    /*
     

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?

     */
    public class Task7 : ITask
    {
        public void Calculate()
        {
            var primes = GeneratePrimes(10001);
            Console.WriteLine("Found {0}", primes[primes.Length-1]);
        }

        private static long[] GeneratePrimes(long n)
        {
            var index = 0;
            var primes = new long[n];
            primes[index++] = 2;
            var nextPrime = 3;
            while (index < n)
            {
                var sqrt = (long)Math.Sqrt(nextPrime);
                var isPrime = true;
                for (var i = 0; primes[i] <= sqrt; i++)
                {
                    if (nextPrime % primes[i] != 0) continue;
                    isPrime = false;
                    break;
                }
                if (isPrime)
                {
                    primes[index++] = nextPrime;
                }
                nextPrime += 2;
            }
            return primes;
        }
    }
}