﻿using System;

namespace euler
{
    /*
     2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
     What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
     */
    public class Task5 : ITask
    {
        private const int LatestNum = 20;
        public void Calculate()
        {
            long result = 0;
            Console.WriteLine("Max is {0}", Max(LatestNum));
            for (long i = 1; i < Max(LatestNum); i++)
            {
                if (AreAllFactors(i, LatestNum))
                {
                    result = i;
                    break;
                }
            }
            Console.WriteLine("Min divisible is {0}", result);
        }

        private long Max(int i)
        {
            long max = 1;
            for (int j = 1; j <= i; j++)
            {
                max = max*j;
            }
            return max;
        }

        private bool AreAllFactors(long num, int max)
        {
            for (int j = 1; j <= max; j++)
            {
                if (num%j != 0) return false;
            }
            return true;
        }
    }
}