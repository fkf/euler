﻿using System;
using System.Collections.Generic;
using System.Linq;
using euler.Combinatorics;

namespace euler
{
    /// <summary>
    /// TODO: Document Task23
    /// Author: us-fifi
    /// Date: 30.12.2014 10:03:05
    /// </summary>
    public class Task23 : ITask
    {
        public void Calculate()
        {
            const int max = 28123;
            int sum = 0;
            var ab = new List<int>();
            for (int i = 1; i <= max; i++)
            {
                var b = Factors(i).Sum();
                if (b > i)
                    ab.Add(i);
            }
            Console.WriteLine("Generated {0} abundant numbers", ab.Count);
            bool[] isSum = new bool[max+1];
            for (int a = 0; a < ab.Count; a++)
            {
                for (int b = a; b < ab.Count; b++)
                {
                    if (ab[a] + ab[b] <= max) 
                        isSum[ab[a] + ab[b]] = true;
                    else
                        break;
                }
            }
            for (int i = 0; i <= max; i++)
            {
                if (!isSum[i])
                    sum += i;
            }
            Console.WriteLine("Found {0}", sum);
        }

        public List<int> Factors(int number)
        {
            List<int> factors = new List<int>();
            for (int factor = 1; factor < number; factor++)
            {
                if (number % factor == 0)
                {
                    factors.Add(factor);
                }
            }
            return factors;
        }
    }
}