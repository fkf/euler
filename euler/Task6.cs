﻿using System;

namespace euler
{
    /*
The sum of the squares of the first ten natural numbers is,
12 + 22 + ... + 102 = 385

The square of the sum of the first ten natural numbers is,
(1 + 2 + ... + 10)2 = 552 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
     */
    public class Task6 : ITask
    {
        public void Calculate()
        {
            const int threshold = 100;
            var sum2 = (1 + threshold) * (threshold / 2);
            var sum1 = 0;
            for (int i = 1; i <= threshold; i++)
            {
                sum1 += i*i;
            }
            Console.WriteLine("Found {0}", sum2*sum2 - sum1);
        }
    }
}