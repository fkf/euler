﻿namespace euler
{
    public interface ITask
    {
        void Calculate();
    }
}