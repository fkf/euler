﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace euler
{
    /// <summary>
    /// TODO: Document Task21
    /// Author: us-fifi
    /// Date: 30.12.2014 09:09:11
    /// </summary>
    public class Task21 : ITask
    {
        public void Calculate()
        {
            var a = Factor(284);
            foreach (var i in a)
            {
                Console.WriteLine("284 {0}", i);
            }
            
            long sum = 0;
            for (long i = 1; i < 10000; i++)
            {
                var b = Factor(i).Sum();
                if (Factor(b).Sum() == i && b != i)
                {
                    Console.WriteLine("Num {0} and {1}", i, b);
                    sum = sum + i;
                }
            }
            Console.WriteLine("Found {0}", sum);
        }

        public List<long> Factor(long number)
        {
            List<long> factors = new List<long>();
            long max = (long)Math.Sqrt(number);  //round down
            for (long factor = 1; factor <= max; ++factor)
            { //test from 1 to the square root, or the long below it, inclusive.
                if (number % factor == 0)
                {
                    factors.Add(factor);
                    if (factor != max && factor != 1)
                    { // Don't add the square root twice!  Thanks Jon
                        factors.Add(number / factor);
                    }
                }
            }
            return factors;
        }
    }
}