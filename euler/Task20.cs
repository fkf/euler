﻿using System;
using System.Linq;
using System.Numerics;

namespace euler
{
    /// <summary>
    /// TODO: Document Task20
    /// Author: us-fifi
    /// Date: 28.12.2014 09:32:56
    /// </summary>
    public class Task20 : ITask
    {
        public void Calculate()
        {
            BigInteger factorial = 1;
            for (int i = 1; i <= 100; i++)
            {
                factorial *= i;
            }
            var sum = factorial.ToString("D").ToCharArray().Sum(x => char.GetNumericValue(x));
            Console.WriteLine("Found {0}", sum);
        }
    }
}