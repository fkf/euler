﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

namespace euler
{
    /// <summary>
    /// TODO: Document Task24
    /// Author: us-fifi
    /// Date: 30.12.2014 10:45:05
    /// </summary>
    public class Task24 : ITask
    {
        private string _cur = "";
        private List<string> _numbers = new List<string>();

        private void GenerateNumbers(int at)
        {
            if (at == 10)
            {
                // we're at the end of the ranges vector, print the number
                _numbers.Add(_cur);
                return;
            }
            for (var i = 0; i <= 9; ++i)
            {
                // find only unique digits
                if (_cur.Contains(i.ToString())) continue;

                // add the digit for this range to the string
                _cur += i.ToString(CultureInfo.InvariantCulture);

                // recursive call
                GenerateNumbers(at + 1);

                // erase the last digit we added (this is where the backtracking part comes in)
                _cur = _cur.Remove(_cur.Length - 1);
                //_cur.erase(_cur.end() - 1);
            }
        }

        public void Calculate()
        {
            GenerateNumbers(0);
            var arr = _numbers.ToArray();
            Array.Sort(arr);
            Console.WriteLine("Found {0}", _numbers[1000000-1]);
        }
    }


}