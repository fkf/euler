﻿using System;
using System.Linq;
using System.Numerics;

namespace euler
{
    /// <summary>
    /// TODO: Document Task16
    /// Author: us-fifi
    /// Date: 26.12.2014 18:45:31
    /// </summary>
    public class Task16 : ITask
    {
        public void Calculate()
        {
            BigInteger num = BigInteger.Pow(2, 1000);
            var sum = num.ToString("D").ToCharArray().Sum(x => char.GetNumericValue(x));
            Console.WriteLine("Found {0}", sum);
        }
    }
}