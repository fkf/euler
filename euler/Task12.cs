﻿using System;
using System.Collections.Generic;

namespace euler
{
    /// <summary>
    /// TODO: Document Task12
    /// Author: us-fifi
    /// Date: 25.12.2014 22:17:03
    /// </summary>
    public class Task12 : ITask
    {
        public void Calculate()
        {
            long num = 0;
            long counter = 0;
            while (true)
            {
                num += counter;
                if (Factor(num).Count >= 500) break;
                counter++;
            }
            Console.WriteLine("Found {0}", num);
        }

        public List<long> Factor(long number)
        {
            List<long> factors = new List<long>();
            long max = (long)Math.Sqrt(number);  //round down
            for (long factor = 1; factor <= max; ++factor)
            { //test from 1 to the square root, or the int below it, inclusive.
                if (number % factor == 0)
                {
                    factors.Add(factor);
                    if (factor != max)
                    { // Don't add the square root twice!  Thanks Jon
                        factors.Add(number / factor);
                    }
                }
            }
            return factors;
        }
    }
}