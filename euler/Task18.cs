﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace euler
{
    /// <summary>
    /// TODO: Document Task18
    /// Author: us-fifi
    /// Date: 27.12.2014 12:38:42
    /// </summary>
    public class Task18 : ITask
    {
        //private int[][] _p = new int[][]
        //{
        //    new[] {3},
        //    new[] {7, 4},
        //    new[] {2, 4, 6},
        //    new[] {8, 5, 9, 3}
        //};

        private int[][] _p = new int[][]
            {
                new int[] {75},
                new int[] {95,64},
                new int[] {17,47,82},
                new int[] {18,35,87,10},
                new int[] {20,04,82,47,65},
                new int[] {19,01,23,75,03,34},
                new int[] {88,02,77,73,07,63,67},
                new int[] {99,65,04,28,06,16,70,92},
                new int[] {41,41,26,56,83,40,80,70,33},
                new int[] {41,48,72,33,47,32,37,16,94,29},
                new int[] {53,71,44,65,25,43,91,52,97,51,14},
                new int[] {70,11,33,28,77,73,17,78,39,68,17,57},
                new int[] {91,71,52,38,17,14,91,43,58,50,27,29,48},
                new int[] {63,66,04,68,89,53,67,30,73,16,69,87,40,31},
                new int[] {04,62,98,27,23,09,70,98,73,93,38,53,60,04,23}
            };

        public void Calculate()
        {
            var sums = Sum(0, 0);
            Console.WriteLine("Found {0}", sums.Max());


            //Console.WriteLine("Found {0}", sum);

        }
        /// <summary>
        /// Bruteforce method
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <returns></returns>
        private List<int> Sum(int i, int j)
        {
            if (i == _p.Length - 1)
            {
                return new List<int> { _p[i][j] };
            }
            var result = Sum(i + 1, j);
            result.AddRange(Sum(i+1, j+1));
            return result.Select(x => x + _p[i][j]).ToList();
        }

        /// <summary>
        /// Not working method
        /// </summary>
        /// <returns></returns>
        private int Sum1()
        {
            var sum = _p[0][0];
            var idx = 0;
            for (int i = 1; i < _p.Length - 1; i++)
            {
                var maxsum = 0;
                for (int j = 0; j < 2; j++)
                {
                    var idxtmp = 0;
                    for (int k = 0; k < 2; k++)
                    {
                        var tmp = _p[i][idx + j] + _p[i + 1][idx + j + k];
                        if (tmp > maxsum)
                        {
                            idxtmp = idx + j;
                            maxsum = tmp;
                        }
                    }
                    idx = idxtmp;
                }
                sum += (i == _p.Length - 2 ? maxsum : _p[i][idx]);
            }
            return sum;
        }
    }
}