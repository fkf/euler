﻿using System;

namespace euler
{
    /// <summary>
    /// TODO: Document Task14
    /// Author: us-fifi
    /// Date: 25.12.2014 22:38:21
    /// </summary>
    public class Task14 : ITask
    {
        public void Calculate()
        {
            var max = 0;
            long num = 0;
            for (long i = 1; i <= 1000000; i++)
            {
                var chain = GetChain(i);
                if (chain >= max)
                {
                    max = chain;
                    num = i;
                }
            }
            Console.WriteLine("Found {0} with length {1}", num, max);
        }

        private int GetChain(long i)
        {
            var count = 1;
            while (i > 1)
            {
                count++;
                if (i%2 == 0)
                {
                    i = i/2;
                }
                else
                {
                    i = 3*i + 1;
                }
            }
            return count;
        }
    }
}