﻿using System;

namespace euler
{
    /*
        A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
        a2 + b2 = c2

        For example, 32 + 42 = 9 + 16 = 25 = 52.

        There exists exactly one Pythagorean triplet for which a + b + c = 1000.
        Find the product abc.
     */
    public class Task9 : ITask
    {
        public void Calculate()
        {
            for (int a = 0; a < 1000; a++)
            {
                for (int b = 0; b < 1000; b++)
                {
                    var c = 1000 - a - b;
                    if (a >= b || b >= c || a >= c) continue;
                    if (a*a + b*b == c*c)
                    {
                        Console.WriteLine("Found {0} {1} {2}", a, b, c);
                    }
                }
            }
        }
    }
}