﻿using System;
using System.Globalization;

namespace euler
{
    /*
     A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
     Find the largest palindrome made from the product of two 3-digit numbers.
     */
    public class Task4 : ITask
    {
        public void Calculate()
        {
            var palindrom = 0;
            for (int i = 999; i > 100; i--)
            {
                for (int j = 999; j > 100; j--)
                {
                    var num = i*j;
                    if (!IsPalindrom(num.ToString(CultureInfo.InvariantCulture))) continue;
                    if (num > palindrom)
                        palindrom = num;
                }
            }
            Console.WriteLine("Max palindrom is {0}", palindrom);
        }

        public string Reverse(string text)
        {
            if (text == null) return null;

            // this was posted by petebob as well 
            char[] array = text.ToCharArray();
            Array.Reverse(array);
            return new String(array);
        }

        private bool IsPalindrom(string word)
        {
            return word == Reverse(word);
        }
    }
}