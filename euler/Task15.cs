﻿using System;
using System.Collections.Generic;

namespace euler
{
    /// <summary>
    /// TODO: Document Task15
    /// Author: us-fifi
    /// Date: 26.12.2014 18:05:45
    /// </summary>
    public class Task15 : ITask
    {
        public void Calculate()
        {
            const int gridSize = 20;

            long paths = 1;

            for (int i = 0; i < gridSize; i++)
            {
                paths *= (2 * gridSize) - i;
                paths /= i + 1;
            }
            Console.WriteLine("Found {0}", paths);
        }
    }
}