﻿using System;
using System.Diagnostics;

namespace euler
{
    class Program
    {
        static void Main(string[] args)
        {
            var watch = new Stopwatch();
            watch.Start();
            ITask task = new Task23();
            task.Calculate();
            watch.Stop();
            Console.WriteLine("Time elapsed {0}", watch.Elapsed);
            Console.ReadKey();
        }
    }
}
