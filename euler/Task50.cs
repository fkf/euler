﻿using System;

namespace euler
{
    /// <summary>
    /// TODO: Document Task50
    /// Author: us-fifi
    /// Date: 28.12.2014 09:38:29
    /// </summary>
    public class Task50 : ITask
    {
        private const int GeneratedPrimes = 1000000;
        private readonly int[] _primes;
        public Task50()
        {
            _primes = GeneratePrimes(GeneratedPrimes);
        }

        public void Calculate()
        {
            var idx = 0;
            long max = 0;
            int maxprime = 0;
            while (_primes[idx] < GeneratedPrimes)
            {
                var result = TryIndex(idx, _primes[idx]);
                if (result > max)
                {
                    max = result;
                    maxprime = _primes[idx];
                }
                idx++;
            }
            Console.WriteLine("Found prime {0} with max sequence {1}", maxprime, max);
        }

        private long TryIndex(int idx, int max)
        {
            for (int from = idx -1; from >= 0; from--)
            {
                var i = from;
                long sum = 0;
                while (sum < max)
                {
                    sum += _primes[i];
                    i++;
                }
                if (sum == max) return i - from;
            }
            return 0;
        }

        private static int[] GeneratePrimes(int n)
        {
            var index = 0;
            var primes = new int[n];
            primes[index++] = 2;
            var nextPrime = 3;
            while (index < n)
            {
                var sqrt = (int)Math.Sqrt(nextPrime);
                var isPrime = true;
                for (var i = 0; (int)primes[i] <= sqrt; i++)
                {
                    if (nextPrime%primes[i] != 0) continue;
                    isPrime = false;
                    break;
                }
                if (isPrime)
                {
                    primes[index++] = nextPrime;
                }
                nextPrime += 2;
            }
            return primes;
        }
    }
}