﻿using System;
using System.IO;
using System.Runtime.Remoting.Channels;
using System.Threading;

namespace euler
{
    /// <summary>
    /// TODO: Document Task17
    /// Author: us-fifi
    /// Date: 26.12.2014 18:52:26
    /// </summary>
    public class Task17 : ITask
    {
        public void Calculate()
        {
            var letters = 0;
            for (int i = 1; i <= 1000; i++)
            {
                letters += Count(i).Length;
                //Console.WriteLine(Count(i));
            }
            Console.WriteLine("Found {0}", letters);
        }

        private string Count(int i)
        {
            var result = "";
            var str = i.ToString();
            for (int j = 0 ; j < str.Length; j++)
            {
                var t = "";
                if (str.Length - j != 2) t = GetText(str[j]);
                else
                {
                    if (str[j] == '1')
                    {
                        t = GetText2(str.Substring(str.Length - Math.Min(2, str.Length)));
                        if (str.Length > 2 && !string.IsNullOrEmpty(t)) 
                            result += "and";
                        return result + t;
                    }
                    t = GetText1(str[j]);
                    if (str.Length > 2 && (!string.IsNullOrEmpty(t)||!string.IsNullOrEmpty(GetText(str[j+1])))) result += "and";
                }


                if (str.Length - j == 3 && !string.IsNullOrEmpty(t)) t += "hundred";
                if (str.Length - j == 4 && !string.IsNullOrEmpty(t)) t += "thousand";
                result += t;

            }
            return result;
        }

        private string GetText(char ch)
        {
            switch (ch)
            {
                case '1': return "one";
                case '2': return "two";
                case '3': return "three";
                case '4': return "four";
                case '5': return "five";
                case '6': return "six";
                case '7': return "seven";
                case '8': return "eight";
                case '9': return "nine";
            }
            return "";
        }

        private string GetText2(string ch)
        {
            switch (ch)
            {
                case "10": return "ten";
                case "11": return "eleven";
                case "12": return "twelve";
                case "13": return "thirteen";
                case "14": return "fourteen";
                case "15": return "fifteen";
                case "16": return "sixteen";
                case "17": return "seventeen";
                case "18": return "eighteen";
                case "19": return "nineteen";
            }
            return "";
        }

        private string GetText1(char ch)
        {
            switch (ch)
            {
                case '2': return "twenty";
                case '3': return "thirty";
                case '4': return "forty";
                case '5': return "fifty";
                case '6': return "sixty";
                case '7': return "seventy";
                case '8': return "eighty";
                case '9': return "ninety";
            }
            return "";
        }
    }
}