﻿using System;

namespace euler
{
    /*
     The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
     */
    public class Task10 : ITask
    {
        public void Calculate()
        {
            var primes = GeneratePrimes(2000000);
            long sum = 0;
            var i = 0;
            while (primes[i] < 2000000)
            {
                sum += primes[i];
                i++;
            }
            Console.WriteLine("Found {0}", sum);
        }

        private static long[] GeneratePrimes(long n)
        {
            var index = 0;
            var primes = new long[n];
            primes[index++] = 2;
            var nextPrime = 3;
            while (index < n)
            {
                var sqrt = (long)Math.Sqrt(nextPrime);
                var isPrime = true;
                for (var i = 0; primes[i] <= sqrt; i++)
                {
                    if (nextPrime % primes[i] != 0) continue;
                    isPrime = false;
                    break;
                }
                if (isPrime)
                {
                    primes[index++] = nextPrime;
                }
                nextPrime += 2;
            }
            return primes;
        }
    }
}