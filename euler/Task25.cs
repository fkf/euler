﻿using System;
using System.Numerics;

namespace euler
{
    /// <summary>
    /// TODO: Document Task25
    /// Author: us-fifi
    /// Date: 28.12.2014 17:09:14
    /// </summary>
    public class Task25 : ITask
    {
        public void Calculate()
        {
            BigInteger prev = 1;
            BigInteger num = 2;
            var count = 3;
            while (num.ToString("D").Length < 1000)
            {
                var tmp = num;
                num = num + prev;
                prev = tmp;
                count++;
            }
            Console.WriteLine("Found {0}", count);
        }
    }
}