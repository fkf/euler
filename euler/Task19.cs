﻿using System;

namespace euler
{
    /// <summary>
    /// TODO: Document Task19
    /// Author: us-fifi
    /// Date: 28.12.2014 09:28:30
    /// </summary>
    public class Task19 : ITask
    {
        public void Calculate()
        {
            var count = 0;
            for (var i = 1901; i <= 2000; i++)
            {
                for (var j = 1; j <= 12; j++)
                {
                    if (new DateTime(i, j, 1).DayOfWeek == DayOfWeek.Sunday) count++;
                }
            }
            Console.WriteLine("Found {0}", count);
        }
    }
}