﻿using System;

namespace euler
{
    /*
     The prime factors of 13195 are 5, 7, 13 and 29.
     What is the largest prime factor of the number 600851475143 ?
     */
    public class Task3 : ITask
    {
        public void Calculate()
        {
            long num = 600851475143;
            long max = (long)Math.Sqrt(num); // max
            long maxprime = 0;
            for(long factor = max; factor > 1; factor--) {
                if(num % factor == 0 && IsPrime(factor))
                {
                    maxprime = factor;
                    break;
                }
            }
            
            Console.WriteLine("Max prime factor is {0}", maxprime);
        }

        private static bool IsPrime(long number)
        {

            if (number == 1) return false;
            if (number == 2) return true;

            for (var i = 2; i <= Math.Ceiling(Math.Sqrt(number)); ++i)
            {
                if (number % i == 0) return false;
            }

            return true;
        }
    }
}